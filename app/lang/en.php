<?php

return [
    'error.missing' => 'Field not found',
    'error.object_not_found' => '%s not found',
    'error.type_mismatch' => 'Type mismatch',
    'error.not_unique' => 'The field already used',
    'error.empty' => 'The field is empty',
    'amount.not_positive' => 'The amount must be positive',
    'amount.insufficiently' => 'Insufficient funds',
    'rpcResponseError' => [
        -32700 => 'Parse error',
        -32600 => 'Invalid Request',
        -32601 => 'Method not found',
        -32602 => 'Invalid params',
        -32603 => 'Internal error',
        -32000 => 'Server error',
    ],
];