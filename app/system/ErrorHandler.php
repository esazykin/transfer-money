<?php

namespace app\system;

use Whoops\Exception\ErrorException;
use Whoops\Handler\Handler;

class ErrorHandler extends Handler
{
    /**
     * @var boolean
     */
    private $debug;

    /**
     * @param $debug boolean
     */
    public function __construct($debug)
    {
        $this->debug = $debug;
    }

    /**
     * @return int|null A handler may return nothing, or a Handler::HANDLE_* constant
     */
    public function handle()
    {
        /** @var ErrorException $exception */
        $exception = $this->getException();
        $errorScope = [
            'datetime' => date("Y-m-d H:i:s (T)"),
            'errMsg' => $exception->getMessage(),
            'file' => $exception->getFile(),
            'line' => $exception->getLine(),
//			'vars' => isset($vars['HTTP_RAW_POST_DATA']) ? $vars['HTTP_RAW_POST_DATA'] : '',
        ];
        $errorMessage = json_encode($errorScope, JSON_PRETTY_PRINT);
        $r = error_log("$errorMessage,", 3, projectPath() . "/runtime/" . date("Y-m-d") . "_error.log");
        if ($this->debug) {
            echo $errorMessage;
        }
    }
}