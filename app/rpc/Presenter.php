<?php

namespace app\rpc;

use app\rpc\request\RpcRequestBase;
use app\rpc\response\RpcResponse;
use app\rpc\validator\RpcRequestValidatorBase;
use Doctrine\ORM\EntityManager;

class Presenter
{
    /**
     * @var RpcRequestValidatorBase
     */
    private $validator;

    /**
     * @var string
     */
    private $methodNamespace;

    /**
     * @var EntityManager
     */
    private $doctrine;

    public function __construct(RpcRequestValidatorBase $validator, $methodNamespace, EntityManager $doctrine)
    {
        $this->validator = $validator;
        $this->methodNamespace = $methodNamespace;
        $this->doctrine = $doctrine;
    }

    public function process(RpcRequestBase $request)
    {
        if ($this->validator->validate($request)) {
            $cls = $this->methodNamespace . '\\' . ucfirst($request->getMethod()) . 'Method';
            $body = (new $cls($this->doctrine))->run($request->getParams());
        } else {
            $body = $this->validator->getError();
        }
        return new RpcResponse($request->getId(), $request->getVersion(), $body);
    }
}