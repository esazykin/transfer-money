<?php

namespace app\rpc\request;

interface RpcRequestBase
{
    public function getId();

    public function getVersion();

    public function getMethod();

    public function getParams();
}