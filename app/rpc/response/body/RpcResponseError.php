<?php

namespace app\rpc\response\body;

/**
 * Class RpcAnswerError
 * @package app
 * @link http://www.jsonrpc.org/specification#error_object
 */
class RpcResponseError implements RpcResponseBody
{
    const CODE_PARSE_ERROR = -32700;
    const CODE_INVALID_REQUEST = -32600;
    const CODE_METHOD_NOT_FOUND = -32601;
    const CODE_INVALID_PARAMS = -32602;
    const CODE_INTERNAL_ERROR = -32603;
    const CODE_SERVER_ERROR = -32000;

    /**
     * @var int
     */
    private $code;

    /**
     * @var array
     */
    private $data;

    /**
     * @return array
     */
    public function getBody()
    {
        $messages = lang('rpcResponseError');
        $body = [
            'code' => $this->code,
            'message' => isset($messages[$this->code]) ? $messages[$this->code] : '',
        ];
        if (!empty($this->data)) {
            $body['data'] = $this->data;
        }
        return ['error' => $body];
    }

    /**
     * @param int $code
     */
    public function setCode($code)
    {
        $this->code = $code;
    }

    /**
     * @param array $data
     */
    public function setData($data)
    {
        $this->data = $data;
    }
}