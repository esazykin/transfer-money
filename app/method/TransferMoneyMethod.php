<?php

namespace app\method;

use app\domain\model\User;
use app\rpc\response\body\RpcResponseBody;
use app\rpc\response\body\RpcResponseResult;
use Exception;

class TransferMoneyMethod extends Method
{
    /**
     * @param array $params
     * @return RpcResponseBody
     */
    public function run(array $params)
    {
        if (empty($params['userFrom'])) {
            return $this->createError(['userFrom' => ['missing' => lang('error.missing')]]);
        }
        if (empty($params['userTo'])) {
            return $this->createError(['userTo' => ['missing' => lang('error.missing')]]);
        }
        if (empty($params['amount'])) {
            return $this->createError(['amount' => ['missing' => lang('error.missing')]]);
        }
        $amount = (float)$params['amount'];
        if ($amount <= 0) {
            return $this->createError(['amount' => ['not_positive' => lang('amount.not_positive')]]);
        }

        $userRepository = $this->doctrine->getRepository(User::class);
        /** @var User $userFrom */
        $userFrom = $userRepository->find((int)$params['userFrom']);
        if (!$userFrom) {
            return $this->createError(['userFrom' => ['object_not_found' => sprintf(lang('error.object_not_found'), 'User')]]);
        }
        /** @var User $userTo */
        $userTo = $userRepository->find((int)$params['userTo']);
        if (!$userTo) {
            return $this->createError(['userTo' => ['object_not_found' => sprintf(lang('error.object_not_found'), 'User')]]);
        }
        
        $userFrom->decrByBalance($amount);
        $userTo->incrByBalance($amount);
        if ($userFrom->getBalance() < 0) {
            return $this->createError(['userFrom' => ['insufficient_funds' => lang('amount.insufficiently')]]);
        }

        $this->doctrine->beginTransaction();
        try {
            
            $this->doctrine->persist($userFrom);
            $this->doctrine->persist($userTo);
            $this->doctrine->flush();
            $this->doctrine->getConnection()->commit();
        } catch (Exception $e) {
            $this->doctrine->rollback();
            return $this->createError(['code' => ['unknown' => $e->getMessage()]]);
        }

        return new RpcResponseResult([
            'userFrom' => $userFrom->getAttributes(),
            'userTo' => $userTo->getAttributes(),
        ]);
    }
}
