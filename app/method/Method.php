<?php

namespace app\method;


use app\rpc\response\body\RpcResponseError;
use Doctrine\ORM\EntityManager;

abstract class Method
{
    /**
     * @var EntityManager
     */
    protected $doctrine;

    public function __construct(EntityManager $doctrine)
    {
        $this->doctrine = $doctrine;
    }

    abstract public function run(array $params);

    protected function getMethodName()
    {
        $cls = self::class;
        return substr($cls, strrpos($cls, '\\') + 1);
    }

    protected function createError(array $data)
    {
        $body = new RpcResponseError();
        $body->setCode(RpcResponseError::CODE_INVALID_PARAMS);
        $body->setData($data);
        return $body;
    }
}