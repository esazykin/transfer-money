<?php

namespace app\domain\form;


use app\domain\exception\PopulateException;
use app\domain\model\Model;

abstract class Form
{
	/**
	 * @var Model
	 */
	protected $model;

	/**
	 * @var string
	 */
	protected $modelClass;

	public function __construct()
	{
		$this->model = new $this->modelClass();
	}

	/**
	 * @param array $params
	 * @throws PopulateException
	 */
	public function setAttributes(array $params)
	{
		foreach ($params as $field => $value) {
			$method = 'set' . ucfirst($field);
			if (method_exists($this, $method)) {
				$this->$method($value);
				unset($params[ $field ]);
			}
		}
		$this->model->populate($params);
	}

	abstract public function getCriteria();
}