<?php

namespace app\domain\model;

use app\domain\exception\PopulateException;

abstract class Model
{
    /**
     * @Id @Column(type="integer") @GeneratedValue
     * @var int
     */
    protected $id;

    public function getId()
    {
        return $this->id;
    }

    /**
     * @param array $params
     * @throws PopulateException
     */
    public function populate(array $params)
    {
        foreach ($params as $field => $value) {
            $method = 'set' . ucfirst($field);
            if (!method_exists($this, $method)) {
                throw new PopulateException($field);
            }
            $this->$method($value);
        }
    }

    public function getAttributes()
    {
        $r = [];
        foreach (get_object_vars($this) as $field => $value) {
            $method = 'get' . ucfirst($field);
            if (method_exists($this, $method)) {
                $r[$field] = $this->$method();
            }
        }
        return $r;
    }
}