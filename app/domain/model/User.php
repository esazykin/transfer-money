<?php

namespace app\domain\model;

/**
 * @Entity(repositoryClass="app\data\UserRepository")
 * @Table(
 *      name="users",
 * 		indexes={
 * 			
 * 		}
 * )
 */
class User extends Model
{
    /**
	 * @Column(type="string", length=255)
     * @var string
     */
    protected $name;

    /**
	 * @Column(type="decimal", precision=10, scale=2)
     * @var float
     */
    protected $balance;

    public function getBalance()
    {
        return $this->balance;
    }

    public function setBalance($balance)
    {
        $this->balance = $balance;
    }

    public function incrByBalance($amount)
    {
        $this->balance += $amount;
    }

    public function decrByBalance($amount)
    {
        $this->balance -= $amount;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
    }
}
