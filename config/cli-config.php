<?php

use Doctrine\ORM\Tools\Console\ConsoleRunner;
use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;

$projectPath = getcwd();

require_once "$projectPath/vendor/autoload.php";

$config = Setup::createAnnotationMetadataConfiguration(["$projectPath/app/domain/model"], true);

// obtaining the entity manager
$entityManager = EntityManager::create(require_once "$projectPath/app/config/db.php", $config);

return ConsoleRunner::createHelperSet($entityManager);