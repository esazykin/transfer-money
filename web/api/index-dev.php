<?php

use Silex\Application;

/** @var Application $app */
$app = require_once('boot.php');
$app['debug'] = true;
$app->get('/', function() use ($app) {
    return $app->stream(function () {
        readfile(projectPath() . '/app/template/debug-form.html');
    }, 200, ['Content-Type' => 'text/html']);
});

require_once('run.php');