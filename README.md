###Задание###

CREATE TABLE `users` (
`id` int(10) unsigned NOT NULL AUTO_INCREMENT,
`name` varchar(255) NOT NULL,
`balance` decimal(10,2) NOT NULL,
PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;


Используя таблицу users, реализовать перечисление денежных средств с баланса одного
пользователя на баланс другого. Задачу необходимо решить на одном из
PHP­фреймворков (на свой выбор).